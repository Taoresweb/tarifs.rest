// Base Model Setup
// ============================================================================
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// Model Schema
// ============================================================================

var tarifsSchema = new Schema({
    product: String,
    price: Number
});


// Init
// ============================================================================
module.exports = mongoose.model('tarifs', tarifsSchema);