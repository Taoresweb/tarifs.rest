// Base Setup
// ============================================================================
var express = require('express');
var tarifs = express.Router();
var tarifsDB = require('../models/tarifs');
var wrapper = require('../tools/wrapper.js');
var host = 'http://localhost:8080/read/';
var tarif = new tarifsDB();


// Index Routes
// ============================================================================

/* Catch-all route to set global values */
tarifs.use( function (req, res, next) {
    res.type('application/json');
    res.locals.wrap = wrapper.create({ start: new Date() });
    next();
});

/* GET (Homepage) */
tarifs.get('/', function(req, res) {
    res.render('index', { title: 'Tarifs API' });
});

/* GET (Read) */
tarifs.get('/read', function(req, res){
    tarifsDB.find(function (err, tarifs) {
        if (err) {
            res.json(500, { error: err });
            return;
        }
        res.json(200, res.locals.wrap({ item: tarifs.map(function (tarifs) {
            return host + tarifs._id;
        })}))
    });
});

tarifs.get('/read/:_id', function(req, res) {

    tarifsDB.findById(req.params._id, function (err, tarifs) {
        if (err) {
            res.json(500, { error: err });
            return;
        }

        if (!tarifs) {
            res.json(404, { error: { message: "We did not find a product with the id " + req.params._id }});
            return;
        }

        res.json(200, res.locals.wrap({tarifs}, { self: host + req.params._id }));
    });
});


// Init
// ============================================================================
module.exports = tarifs;