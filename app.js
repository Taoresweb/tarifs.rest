// Base Setup
// ============================================================================
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require('debug')('tarifs.rest');
var mongoose = require('mongoose');
var app = express();

/* Configure the API */
app.set('views', path.join(__dirname, './app/views'));
app.set('view engine', 'jade');

/* Manage dependencies */
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));

/* Connect to mongoDB */
mongoose.connect('mongodb://localhost/tarifs');

/* Set the port API */
var port = process.env.PORT || 8080;

// API Routes
// ============================================================================
var home = require('./app/home/index');

/* Configure routes */
app.use('/', home);

// Error Handlers
// ============================================================================

/* catch 404 and forwarding to error handler */
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
});

/* development error handler
 will print stacktrace */
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

/* production error handler
 no stacktraces leaked to user */
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// Start The Server
// ============================================================================
app.listen(port);
console.log('tarifs.rest server is listening on port ' + port);